# Management Skills Training - Something You Need To Solve From The First Day

If you find yourself (or others) stating 'that's simply the way I am' or 'it's simply my personality and I can't alter it' you (or those others) have what Stanford University psychologist Carol Dweck in her book Mindset calls a FIXED MINDSET. Here's what she says:

' A fixed frame of mind comes from the belief that your qualities are sculpted in stone - who you are is who you are, duration. Characteristics such as intelligence, character, and creativity are repaired qualities, instead of something that can be established.'

' You can't teach an old dog new tricks' is an expression that springs to mind, here. Or 'I've never ever been any good at ...' Talk about a self-fulfilling prediction!

What inspiring leaders wish to cultivate is what Dweck calls A 'growth state of mind'. With this frame of mind you think that you can change things through effort, practice, and experience. So, yes, you can have [Leadership Presence Training](https://www.corporateclassinc.com/executive-leadership-presence-training-workshops/); yes, you can influence your senior group; yes, you can have a much better work-life balance; yes, you can run reliable, interesting and purposeful conferences; yes, you can deliver fantastic presentations. You can see where I'm choosing this ...

One of the reasons that standard training courses don't constantly result in the wanted changes is due to the fact that they totally disregard frame of mind.

You've most likely participated in a couple of courses where you find out brand-new theories, models, and structures - all very interesting and useful in lots of ways. However, be truthful, you've not really used them back at work.

Or you've sent your staff member on pricey leadership programs which they delight in but (frustratingly) their management ability does not improve one iota.

Or you've sent employees on assertiveness training - they discover a couple of expressions, they find out about body language, they discover 'I'm OK, you're OK' however they disappear assertive than they were in the past. (Ask them about their beliefs, their drivers, their worths, their greatest worries and you'll begin to get to the state of mind behind the behavior).

My favorite example of Focusing on the Wrong Thing is Time Management training. Absolutely nothing wrong with it, per se. But I understand people who can spout every theory setting about time management but still choose to let time manage them. Why?

Since sometimes it's simpler to hang out doing 'safe' things rather than those in the 'challenging' box! (So reading emails is safer than having that hard conversation).

I know enough now about mindset to know when I need to change mine, leave the fence, and stop making excuses. And it can be extremely challenging. I may procrastinate for a while. Since in some cases the fence can be a comfy location to be - however it's uneasy even agonizing after a while.

So here's how to start cultivating a development mindset.

Look at one belief you hold about yourself that is not serving you well. (often starts with I should/I must/I ought ...).

Ask yourself 'what effort, practice, and experience do I require to change that frame of mind?'.

Due to the fact that you can teach an old dog new techniques! And brand-new techniques suggest more benefits!